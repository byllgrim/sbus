# sbus

A simple digital bus.


## System

* clk   - To syncronize everything
* rst   - To abort and restart everything


## Information

Information is passed from master and/or from slave.

* m_out - Master's data/command output
* s_out - Slave's data output

These represent a bundle of other signals (e.g. m_adr, s_tag).


## Handshake

Validity of data is indicated, or a stall is demanded, etc.

* m_snd - Master sends valid output
* s_snd - Slave sends valid output
* m_rcv - Master ready to receive
* s_rcv - Slave ready to receive

A transfer from A to B (m->s, or s->m) is commited to when:
`@(posedge clk) A_snd && B_rcv`


## Reply

When one transfer demands a reply (e.g. master wanna read).

1 or more cycles after a transfer is commited to, a reply can arrive if requested.

* Ordered reply
  * The next incoming transfer is the reply
* Tagged reply
  * m_out has m_tag, s_out has s_tag
  * Reply must match tag of inquiring transfer


## Read / Write

* m_out
  * m_wrt - 1 is write, 0 is read
  * m_dat - Data to be written
  * m_adr - Address
* s_out
  * s_dat - Data being read

A read response arrives some cycle later.


## TODO

* Lane select?
* Address?
* IDs?
* User data?
* Point to point? Crossbar? Daisy chain? Shared bus?
* Combinatorial?
* Single? Pipelined? Burst?
* Stream? Memory mapped? Conduit? Tri state? Interrupt?
* Outstanding?


## Comments

Everything is optional.

Anything not forbidden is permitted.

Waveforms are written for WaveDrom.

A_out means either m_out or s_out.

Inspiration: Obi, Wishbone, Ocp, Avalon, Ahb5.
