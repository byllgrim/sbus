sbus - Simple Bus
================================================================================

Trying to design a modular bus specification.
The goal is to match the feature of any competing bus.
But there must be complete freedom in which parts to support.
The minimal base must be extremely small.

Each module presents a unique set of signals.
When the module is not used, its signals have an assumed default.
Modules can invalidate other modules.

The actors involved are: system, master, slave



Basics
--------------------------------------------------------------------------------

### B - Base  # TODO split into C clock and M master?

```
clk   - System's clock
m_out - Master's aggregate of outputs
```

When posedge of `clk`,
slave obeys the command at `m_out`.

* Default: N/A (this module is mandatory)
* Responsible: system, master



### bR - Reset

```
rst - System's reset
```

When posedge of `clk`,
all other modules are invalid,
and master and slave initializes.

* Default: 0
* Responsible: system, master, slave



### bA - Answer

```
s_out - Slave's aggregate of outputs
```

When posedge of `clk`,
slave gave the answer to the previous `m_out`.

* Default: 0
* Responsible: slave



Command Negotiation
--------------------------------------------------------------------------------

### bS - Stall

```
s_stl - Slave is stalling
```

When `s_stl`,
B semantics are invalid (slave does not obey).

* Default: 0
* Responsible: master, slave



### bV - Valid

```
m_vld - Master's output can be used
```

When `!m_vld`,
B semantics are invalid (master does not command).

* Default: 1  # TODO rename I - Invalid to get default 0?
* Responsible: master, slave



Answer Negotiation
--------------------------------------------------------------------------------

### baD - Declare

```
s_dec - Slave declares its answer
```

When `!s_dec`,
A semantics are invalid (slave gave no answer).

* Default: 1
* Responsible: master, slave



### baM - Mute

```
m_mut - Master mutes slave's answer
```

When `m_mut`,
A semantics are invalid (master does not hear the answer).

* Default: 0
* Responsible: master, slave



Misc
--------------------------------------------------------------------------------

Other thoughts and ideas.

W write-n-read (wre),
L lane-select (sel,)



Conclusion
--------------------------------------------------------------------------------

List of signals (might be out of date!):
```
clk   - System's clock
m_out - Master's aggregate of outputs
rst   - System's reset
s_out - Slave's aggregate of outputs
s_stl - Slave is stalling
m_vld - Master's output can be used
s_dec - Slave declares its answer
m_mut - Master mutes slave's answer
```
